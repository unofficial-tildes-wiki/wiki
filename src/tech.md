---
title: ~tech
intro: Technology news and discussions, intended for a general audience. Posts requiring deep technical knowledge should go in more-specific groups.
...

## Notable Topics

  * [FOSS Alternatives to popular services](tech/foss-alternatives.html)
  * [Recommended Extensions from the Tildes Community](tech/recommended-extensions.html)