---
title: introduction
...

## A general introduction to Tildes

Lots of new folks seem to be coming in these past days, so I wanted to make a post that compiles some useful things to know, commonly asked questions, and a general idea of tildes history (short though it may be). Please keep in mind that [Tildes is still in Alpha](https://tildes.net/~tildes/522/how_will_tildes_work_with_bans#comment-1g87), and many features that are usually present such as repost detection haven’t been implemented yet.

## Settings 

First of all, check out the [Settings](https://tildes.net/settings) if you haven’t yet. It’s located in your user profile, on the right sidebar. There are different themes available. I recommend setting up [account recovery](https://tildes.net/settings/account_recovery) in case you forget your password, and [comment highlighting](https://tildes.net/settings/comment_visits) to highlight new comments in a thread.

## Posting 

You can post a topic by navigating to a group and clicking on the button in the right sidebar. Tildes uses markdown, if you are not familiar with it check the [text formatting doc page](https://docs.tildes.net/text-formatting). Please tag your post so it is easier for other people to find, and check out [the tagging guidelines](https://docs.tildes.net/topic-tagging). Some posts have a topic log in the sidebar that shows what changes were done to the post since it was posted. You can see an example <https://tildes.net/~talk/501/>. Like much rather ephemeral pieces of information, this Topic Log is deleted after thirty days.

## Tags 

You can find all posts with the same tag by clicking on a tag on a post, which will take you to an url like `https://tildes.net/?tag=ask`, where ask is the tag you clicked on. Replace ask with whatever tag you want to search for. You can also filter tags within a group like this: `https://tildes.net/~tildes?tag=discussion`, and it will only show you posts within that group. Clicking on a tag while you are in a group achieves the same effect.

You can also filter out posts with specific tags by going to your settings and [defining topic tag filters](https://tildes.net/settings/filters).

## Default sorting 

The current default sorting is **activity, last 3 days**. Activity sort bumps a post up whenever someone replies to it. ‘Last 3 days’ mean that only posts posted in the past 3 days will be shown. You can change your default sort by choosing a different sort method and/or time period, and clicking the ‘set as default’ button that will appear on the right.

## Extensions 

@Emerald_Knight has compiled a list of user created extensions and CSS themes here: <https://gitlab.com/Emerald_Knight/awesome-tildes>

In particular, I found the browser extension [Tildes Extended](https://github.com/theCrius/tildes-extended) by @crius and @Bauke very useful. It has nifty features like jumping to new comments, markdown preview and user tagging.

## Tildes Development

Tildes is open source and if you want to contribute to Tildes development, this is what you should read: <https://gitlab.com/tildes/tildes/blob/master/CONTRIBUTING.md>

For those who can’t code, you might still be interested in the [issue boards](https://gitlab.com/tildes/tildes/boards) on Gitlab. It contains known issues, features being worked on, and plans for the future. If you have a feature in mind that you want to suggest, try looking there first to see if others have thought of it already, or are working on it.

----

## Tildes’ Design and Mechanics 

In other words, how is it going to be different from reddit? Below are some summaries of future mechanics and inspiration for tildes’ design. Note: most of the mechanics have not been implemented and are subject to change and debate.

  - Tildes will not have conventional moderators. Instead, the moderation duties will be spread to thousands of users by the trust system. More info on how it works and why it is designed that way:
    * [docs on future mechanics](https://docs.tildes.net/mechanics-future)
    * [possible levels of trust that users can acquire](https://www.reddit.com/r/RedditAlternatives/comments/8kez6p/tildes_by_former_reddit_dev_invite_only/dz8yp7y/)
    * [a long rant](https://www.reddit.com/r/listentothis/wiki/siteideas)
    * [A group is it’s worst own enemy](https://tildes.net/~tildes.official/16g/daily_tildes_discussion_a_group_is_its_own_worst_enemy)
    * [this neat game about trust and game theory](https://ncase.me/trust//)
  - Instead of subreddits, there are [groups](https://docs.tildes.net/mechanics#groups), a homage to [Usenet](https://en.wikipedia.org/wiki/Usenet). Groups will be organized hierarchically, the first and only subgroup right now is ~tildes.official. Groups will never be created by a single user, instead, they will be created based on group interest. For example, if a major portion of ~games consists of [DnD](https://en.wikipedia.org/wiki/Dungeons_%26_Dragons) posts and they are drowning out all the other topics, a ~games.dnd subgroup would be created -either by petition, algorithm, or both - to contain the posts, and those who don’t like DnD can unsubscribe from ~games.dnd.
  - Tildes is very [privacy oriented](https://docs.tildes.net/privacy-policy). See:
    * [Haunted by data](https://tildes.net/~tildes.official/2d4/daily_tildes_discussion_haunted_by_data)

----

## Tildes History/Commonly answered questions

I recommend you check out the FAQ and [this past introduction post](https://tildes.net/~tildes/r5) by @Amarok before anything else, it’s a bit outdated but contains many interesting discussions and notable events that have happened on Tildes. @Bauke also tracks noteworthy events each month on his website <https://til.bauke.xyz/>. Also see <https://docs.tildes.net/faq> in the docs. Other than that, the best way for you to get an idea of how tildes changed over time is to go to ~tildes.official and look at all the past daily discussions.

Below are some scattered links that I found interesting, informative, or important:

  * There are 2 announced bans, you can read about them [here](https://tildes.net/~tildes.official/wv/daily_tildes_discussion_our_first_ban), and [here](https://tildes.net/~tildes.official/1wa/daily_tildes_discussion_banning_for_bad_faith_trolling_behavior).
  * [The first group proposal](https://tildes.net/~tildes.official/342/daily_tildes_discussion_proposals_for_trial_groups_round_1), and the resulting [group dditions](https://tildes.net/~tildes.official/3qv/four_new_groups_added_and_everyone_subscribed_anime_enviro_humanities_and_life).
  * You used to be able to [see who invited a user](https://tildes.net/~tildes.official/1zz/daily_tildes_discussion_should_inviter_invitee_info_be_public) on their profile, but [now you can't](https://tildes.net/~tildes.official/26m/invited_by_information_for_users_is_no_longer_displayed).
  * Past introduction posts: [1](https://tildes.net/~talk/dk/introductions), [2](https://tildes.net/~talk/sy/introductions_round_2), [3](https://tildes.net/~talk/10l/i_think_its_time_for_introductions_episode_3), [4](https://tildes.net/~talk/1cl/noticed_we_have_a_lot_of_new_people_so_lets_do_a_new_introduce_yourself_post), [5](https://tildes.net/~talk/4by/who_are_you), [6](https://tild.es/acg)
  * The very [first post](https://tildes.net/~tildes.official/2/welcome_to_tildes) on tildes. 
  * The first page of ~tildes.official, if you want to start reading from [the beginning](https://tildes.net/~tildes.official?after=5k) (there really should be a chronological sorting option) 
  * [What](https://tildes.net/~tildes/k9/what_will_tildes_users_be_called) should [the names](https://tildes.net/~talk/1bt/so_what_should_our_demonym_be) of [Tildes](https://tildes.net/~talk/1bt/so_what_should_our_demonym_be) [users](https://tildes.net/~tildes/27m/what_is_the_collective_term_for_tildes_users) be?
  * [There used to be comment tags](https://tildes.net/~tildes.official/70/daily_tildes_discussion_comment_tags_and_how_they_feel_to_use), but they were [removed because of abuse](https://tildes.net/%7Etildes.official/11n/daily_tildes_discussion_what_do_we_need_to_change_to_make_comment_tags_reasonable_to_re_enable).
  * Some tradition that has started to develop:
    * [programming challenges](https://tildes.net/~comp?tag=challenge)
    * [music weekly threads](https://tildes.net/~music?tag=weekly)
    * [black mirror watch thread](https://tildes.net/~tv/4lz/black_mirror_rewach_announcement_and_schedule)
  * [How do we fund tildes?](https://tildes.net/~tildes/je/lets_talk_about_that_annoying_thing_we_all_dont_want_to_think_about_funding)
  * [How to handle account deletion](https://tildes.net/~tildes.official/2ta/daily_tildes_discussion_how_to_handle_account_deletion)
  * [Anonymous posting](https://tildes.net/~tildes.official/2x3/daily_tildes_discussion_allowing_users_to_post_anonymously)
  * [Demographics survey](https://tildes.net/~tildes/212/demographics_survey_results_year_zero)
  * [How should we deal with low effort content?](https://tildes.net/%7Etildes.official/3t5/daily_tildes_discussion_just_try_to_relax_a_bit)
  * [Why is the comment box at the bottom?](https://tildes.net/%7Etildes/ov/we_gotta_move_the_comment_box_from_the_bottom_of_the_comments_to_the_top)