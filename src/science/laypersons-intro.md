---
title: science/laypersons-intro
intro: A series of posts explaining scientific subjects by various Tildes users.
...

## Notice

This series of threads were initially started by [\@wanda-seldon](https://tildes.net/user/wanda-seldon) and anybody is welcome to make their own contribution on a subject.  

>wanda-seldon said:  
>In an effort to get more content on Tildes, I want to try and give an introduction on several 'hot topics' in semiconductor physics at a level understandable to laypeople (high school level physics background). Making physics accessible to laypeople is a much discussed topic at universities. It can be very hard to translate the professional terms into a language understandable by people outside the field. So I will take this opportunity to challenge myself to (hopefully) create an understandable introduction to interesting topics in modern physics. To this end, I will take liberties in explaining things, and not always go for full scientific accuracy, while hopefully still getting the core concepts across. If a more in-depth explanation is wanted, please ask in the comments and I will do my best to answer.

## Quantum Physics

### Basics of quantum physics

Topic | Date | Subtopics | Author  
---|---|---|---  
[Spin and quantisation part 1](https://tildes.net/~science/831/) | 01 Nov 2018 | [spin](https://tildes.net/~science?tag=spin), [quantisation](https://tildes.net/~science?tag=quantisation) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
[Spin and quantisation part 2](https://tildes.net/~science/863/) | 03 Nov 2018 | [superposition](https://tildes.net/~science?tag=superposition), [observing](https://tildes.net/~science?tag=observing), [collapse](https://tildes.net/~science?tag=collapse) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
[The nature of light and matter part 1](https://tildes.net/~science/8if/) | 16 Nov 2018 | [light](https://tildes.net/~science?tag=light), [matter](https://tildes.net/~science?tag=matter), [wave-particle duality](https://tildes.net/~science?tag=wave_particle_duality), [photoelectric effect](https://tildes.net/~science?tag=photoelectric_effect), [double-slit experiment](https://tildes.net/~science?tag=double_slit_experiment) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
  
### Introductions to advanced topics

Topic | Date | Subtopics | Author  
---|---|---|---  
[Spintronics](https://tildes.net/~science/3j2/) | 18 Jul 2018 | [spintronics](https://tildes.net/~science?tag=spintronics), [electronics](https://tildes.net/~science?tag=electronics), [transistors](https://tildes.net/~science?tag=transistors) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
[Quantum Oscillations](https://tildes.net/~science/7xr/) | 28 Oct 2018 | [quantum oscillations](https://tildes.net/~science?tag=quantum_oscillations) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
[LEDs](https://tildes.net/~science/8d0) | 10 Nov 2018 | [leds](https://tildes.net/~science?tag=leds), [electronics](https://tildes.net/?tag=electronics), [diodes](https://tildes.net/?tag=diodes), [semiconductors](https://tildes.net/?tag=semiconductors) | [\@wanda-seldon](https://tildes.net/user/wanda-seldon)  
  
## Classical physics

### Thermodynamics

Topic | Date | Subtopics | Author  
---|---|---|---  
[Thermodynamics part 1](https://tildes.net/~science/8ao) | 07 Nov 2018 | [energy](https://tildes.net/~science?tag=energy), [work](https://tildes.net/~science?tag=work), [heat](https://tildes.net/~science?tag=heat), [systems](https://tildes.net/~science?tag=systems) | [\@ducks](https://tildes.net/user/ducks)  
[Thermodynamics part 2](https://tildes.net/~science/8fm) | 13 Nov 2018 | [equilibrium](https://tildes.net/~science?tag=equilibrium), [phase changes](https://tildes.net/~science?tag=phase_changes), [ideal gas](https://tildes.net/~science?tag=ideal_gas) | [\@ducks](https://tildes.net/user/ducks)  
[Thermodynamics part 3](https://tildes.net/~science/8qg) | 24 Nov 2018 |  | [\@ducks](https://tildes.net/user/ducks)
