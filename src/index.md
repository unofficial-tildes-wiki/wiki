---
title: start
intro: Thanks for visiting the officially‐unofficial wiki for the preeminent Canadian trust‐based discussion forum named after a punctuation mark on the internet. Until @Deimos gets around to coding an official wiki on‐site, this is what you’re stuck with.
...

## If you’ve just joined Tildes

Welcome! I strongly suggest reading these two links in full:

  * [Announcing Tildes—a non-profit community site driven by its users’ interests](https://blog.tildes.net/announcing-tildes)
  * [A general introduction to Tildes](tildes/introduction.html)

… and at least skimming the [Tildes Docs](https://docs.tildes.net/).

It’s important to remember that, right now, the site only has one administrator and a few thousand users; it is very heavily in alpha, and how it functions now is not necessarily how it will function in the future.

## Contributing to the wiki

If you have something of even minor interest to contribute, either request membership at [the wiki's GitLab organization](https://gitlab.com/unofficial-tildes-wiki), [submit a merge request](https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io/merge_requests/new) with your desired changes or [open an issue](https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io/issues/new). Note that the content of the wiki is hosted in [this repository](https://gitlab.com/unofficial-tildes-wiki/wiki), and that all content issues and MRs should be filed there.

This wiki is currently maintained by [deing](https://tildes.net/user/deing).