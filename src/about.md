---
title: about
intro: Technical and legal details
...

## The Unofficial Tildes Wiki
 * Take a look at the [changelog](changelog.html), which will summarize future content updates.
 * Optionally, you can use a custom [Dracula](https://draculatheme.com/) theme to turn this wiki dark. [Link to usercss.](/res/dracula.user.css)

## Used Fonts
IBM Plex Serif, Copyright © 2017 IBM Corp. with Reserved Font Name "Plex"; licensed under [SIL OFL 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)  
Lato by Łukasz Dziedzic et al.; licensed under [SIL OFL 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)  

## Used technology
Hosted on GitLab Pages. Individual pages are written in Markdown and converted to HTML with Pandoc. Stylesheets are compiled with SASS.  

## Imprint
This page is run by David Lappe; see the <https://15318.de> imprint for further contact information.