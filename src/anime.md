---
title: ~anime
intro: Posts about anime and the related media/genres including TV shows, manga, movies, and games.
...

## Recurring Threads

  * [What have you been watching this week](anime/weekly.html)
  * [This Week in Anime](anime/current.html)