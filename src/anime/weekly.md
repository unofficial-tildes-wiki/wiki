---
title: anime/weekly
intro: Here's where you find out if you're the only one who's rewatched K-ON for the sixth time in a row.
...

## Weekly Thread Archive

Thread currently maintained by @Cleb.

## Notice

All weekly threads can be found [here](https://tildes.net/~anime?order=new&tag=recurring), directly on Tildes.

## 2018 
| Week | Dates | Link |
|--|--|--|
| 43 | 25 Oct. - 30 Oct. | [Link](https://tildes.net/~anime/811/what_have_you_been_watching_reading_this_week_anime_manga) |
| 41 | 9 Oct. - 16 Oct. | [Link](https://tildes.net/~anime/7je/what_have_you_been_watching_reading_this_week_anime_manga)|
| 40 | 2 Oct. - 9 Oct. | [Link](https://tildes.net/~anime/79x/what_have_you_been_watching_reading_this_week_anime_manga) |
| 39 | 25 Sep. - 2 Oct. | [Link](https://tildes.net/~anime/711/what_have_you_been_watching_reading_this_week) |
| 38 | 17 Sep. - 25 Sep. | [Link](https://tildes.net/~anime/6s5/what_have_you_been_watching_reading_this_week) |
| 37 | 9 Sep. - 17 Sep. | [Link](https://tildes.net/~anime/6gk/what_have_you_been_watching_reading_this_week) |
| 36 | 31 Aug. - 9 Sep. | [Link](https://tildes.net/~anime/65j/what_have_you_been_watching_reading_this_week) |
| 35 | 22 Aug. - 31 Aug. | [Link](https://tildes.net/~anime/5t3/what_have_you_been_watching_reading_this_week) |
| 34 | 15 Aug. - 22 Aug. | [Link](https://tildes.net/~anime/5cl/what_have_you_been_watching_reading_this_week) |

