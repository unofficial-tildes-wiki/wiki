---
title: anime/current
intro: ~ Welcome to the storage closet ~
...

## Currently Airing Anime Thread Archive 

Threads currently maintained by @clerical_terrors.

## Notice

The last *This Week in Anime* topic was posted on December 1st, 2018. All threads can be found [here](https://tildes.net/~anime?order=new&tag=currently_airing), directly on Tildes.

## 2018 

| Week Number | Dates | Link |
|--|--|--|
| 44 | 26 Oct. - 2 Nov. | [Link](https://tildes.net/~anime/86d/this_week_in_anime_week_44_of_2018) |
| 43 | 19 Oct. - 26 Oct. | [Link](https://tildes.net/~anime/7xo/this_week_in_anime_week_43_of_2018) |
| 42 | 12 Oct. - 19 Oct. | [Link](https://tildes.net/~anime/7o7/this_week_in_anime_week_42_of_2018) |
| 41 | 5 Oct. - 12 Oct. | [Link](https://tildes.net/~anime/7ex/this_week_in_anime_week_41_of_2018) |